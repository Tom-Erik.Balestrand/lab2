package INF101.lab2;

import java.util.*;

public class Fridge implements IFridge{

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	
	List<FridgeItem> fridgeItems = new ArrayList<>();
	private int fridgeCapasity  = 20;

	@Override
	public int nItemsInFridge() {
		// TODO Auto-generated method stub
		return fridgeItems.size();
	}

	@Override
	public int totalSize() {
		// TODO Auto-generated method stub
		return fridgeCapasity;
	}

	@Override
	public boolean placeIn(FridgeItem item) {
		// TODO Auto-generated method stub
		
		if(nItemsInFridge() < fridgeCapasity) {
			fridgeItems.add(item);
			System.out.println(item);
			return true;
		}
		
		return false;
	}

	@Override
	public void takeOut(FridgeItem item) throws NoSuchElementException{
		// TODO Auto-generated method stub
		if(fridgeItems.isEmpty())throw new NoSuchElementException();
		else fridgeItems.remove(item);
		
	}

	@Override
	public void emptyFridge() {
		// TODO Auto-generated method stub
		fridgeItems.clear();
	}

	@Override
	public List<FridgeItem> removeExpiredFood() {
		// TODO Auto-generated method stub
		List<FridgeItem> removedFridgeItems = new ArrayList<>();
		
		for(int i = 0; i < fridgeItems.size();i++) {
			System.out.println(fridgeItems.get(i));
			if(fridgeItems.get(i).hasExpired()) {
				
				removedFridgeItems.add(fridgeItems.get(i));
			}
		}
		
		fridgeItems.removeAll(removedFridgeItems);
		System.out.println(removedFridgeItems);
		return removedFridgeItems;
	}

}
